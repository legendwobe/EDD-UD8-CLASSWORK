package com.antoni;

import java.util.Random;

public class Sensor {

    public static final double OFFSET = 16;

    public double popNextPressurePsiValue() {

        double valorPresion = muestraPresion();
        return OFFSET + valorPresion;
    }

    private static double muestraPresion() {

        Random basicRandomNumbersGenerator = new Random();

        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();

        return pressureTelemetryValue;
    }


}
