package com.antoni;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;


public class TestAlarm {

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    public void testAlarmIsNotOnByDefault() {

        Alarm alarm = new Alarm(new Sensor());
        Assertions.assertFalse(alarm.isAlarmOn(), "Alarma desactivada por defecto");

    }

    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */

    @Test
    public void testAlarmOnWithLowPressure() {

        Sensor sensor = mock(Sensor.class);

        when(sensor.popNextPressurePsiValue()).thenReturn(Alarm.LOW_PRESSURE_THRESHOLD-1);

        Alarm alarm = new Alarm(sensor);

        alarm.comprobar();

        Assertions.assertTrue(alarm.isAlarmOn(), "Alarma activa");

    }
    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    public void testAlarmOnWithHighPressure() {

        Sensor sensor = mock(Sensor.class);

        when(sensor.popNextPressurePsiValue()).thenReturn(Alarm.HIGH_PRESSURE_THRESHOLD+1);

        Alarm alarm = new Alarm(sensor);

        alarm.comprobar();

        Assertions.assertTrue(alarm.isAlarmOn(), "Alarm activa");

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones normales
     */
    @Test
    public void testAlarmOffWithNormalPressure() {

        Sensor sensor = mock(Sensor.class);

        when(sensor.popNextPressurePsiValue()).thenReturn(Alarm.LOW_PRESSURE_THRESHOLD+2);

        Alarm alarm = new Alarm(sensor);

        alarm.comprobar();

        Assertions.assertFalse(alarm.isAlarmOn(), "Alarma desactivada");

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones límite
     */
    @Test
    public void testAlarmOffWithLimitsPressure() {

        Sensor sensorStub = mock(Sensor.class);
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.HIGH_PRESSURE_THRESHOLD-0.01);
        Alarm alarm = new Alarm(sensorStub);
        alarm.comprobar();
        Assertions.assertFalse(alarm.isAlarmOn(), "Permanece desactivada");

        alarm.comprobar();
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.LOW_PRESSURE_THRESHOLD+0.01);
        Assertions.assertFalse(alarm.isAlarmOn(), "Permanece desactivada");
    }
    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @Test
    public void testAlarmOnWithLimitsPressure() {

        Sensor sensor = mock(Sensor.class);
        when(sensor.popNextPressurePsiValue()).thenReturn(Alarm.HIGH_PRESSURE_THRESHOLD+0.01);

        Alarm alarm = new Alarm(sensor);
        alarm.comprobar();
        Assertions.assertTrue(alarm.isAlarmOn(), "ACTIVADA");

        alarm.comprobar();
        when(sensor.popNextPressurePsiValue()).thenReturn(Alarm.LOW_PRESSURE_THRESHOLD-0.01);
        Assertions.assertTrue(alarm.isAlarmOn(), "ACTIVADA");

    }

}
